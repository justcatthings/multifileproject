# MultiFileProject

This is a template C/C++ project with my own comments, for creating a project using multiple files. Details are explained in the source, with plenty of comments.

A lot of my coding is done with word-wrap turned off, so the comments might be aligned weirdly with anything that wraps text.

Compiles with gcc tools using a process like the one below:

```Shell
# make object MultiFileProject.o
gcc MultiFileProject.cpp -o MultiFileProject.o -c
# make object MyNewHeader.o
gcc MyNewHeader.cpp -o MyNewHeader.o -c
# link objects
gcc -o BinaryName MultiFileProject.o MyNewHeader.o
# run binary
./BinaryName
```
