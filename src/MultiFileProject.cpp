// MultiFileProject.cpp : This is the main program file.
// Even though extensions in the project are .cpp, they should compile as c source too (see the commented/uncommented lines with "For C/For C++")
// This is a demonstration / template file to remember the layout of multi-file projects in c/c++, full of my own comments & thoughts.
// I am not a professional programmer of any kind, so this stuff is solely for my own understanding. There are likely inefficiencies / inaccuracies in here.

#include <stdio.h> // For C
//#include <iostream> // For C++

#include "MyNewHeader.h" // This is a custom header I've included, and will include my own functions into the code here.

/*
* MyNewHeader.h contains forward-declared functions.
*   A forward declaration is essnetially taking something like a function, and only identifying it's interfaces (data inputs and outputs).
*   So if I have a function "int myfunction(int *a, char *b, int c)" which does something with those parameters and returns an int,
*   I only have to define *just* that: 
                                       int myfunction(int *a, char *b, int c);
        (form of a forward-declaration)^^
* 
*   More Info: https://en.wikipedia.org/wiki/Forward_declaration
* 
*   After a forward declaration of this hypothetical function, I would create a similarly-named source file for MyNewHeader.h, or "MyNewHeader.c" (is .cpp in this example)
*   I can then rewrite/expand on that function with its real content. This can reduce clutter when dealing with lots of functions, since you can make libraries of code.
*   In this way you can logically group functions and perhaps other data types/variables together into their own source files as needed, out of view of your "main" code.
* 
*   See MyNewHeader.cpp (the fully defined functions) vs MyNewHeader.h to see how the forward declared functions correlate with the complete functions.
*   Note that the dependencies such as lib & headers for all the code in the .c/.cpp file should be defined in the .h header file.
* 
*
* When compiling a project like this with multiple source files, the process for building main into a single binary varies.
*
*   [VS/VSCode]
*     In Visual Studio you can create and save the .h & .c/.cpp files locally with the main file, or put them in a sub-directory (change your "includes" to reflect any sub-dirs)
*     Then you must go to the solution explorer and right click on source Files choosing "[Add -> Existing Item...] or SHIFT+ALT+A", adding each file you need.
* 
*   [GCC-like tools]
*     With GCC, you will want to first build all the separate .c source files of your project into their object (*.o) files, then link those together (into a binary).
*     This can be automated, but with just a few files it can also be done manually, pretty easy.
* 
*   Generally, the process would follow steps like the one below(in the working dir of the source).
*   [Replace command g++ with gcc (or whatever c compiler being used, like mingw's tools) and then replace .cpp with .c for C source]:
*
*   # (make the two source files in our projcect into objects)
*     g++ MultiFileProject.cpp -o MultiFileProject.o -c
*     g++ MyNewHeader.cpp -o MyNewHeader.o -c
* 
*   # (link the two objects, into executable "BinaryName")
*     g++ -o BinaryName MultiFileProject.o MyNewHeader.o
* 
* The last command when successful will build the objects of the project into the file "BinaryName".
*/

int main() // By including the .h file at the top of this source, main() will be able to interact with the custom MyNewHeader functions
{
    if ( !returnValueInt() ) // Do something if [ NOT returnValueInt() ] is true or nonzero
    {
        printf("!ReturnValueInt() ran, and evaluates to %d \n", !returnValueInt()); // For C
        //std::cout << "ReturnValueInt ran, and returned " << !returnValueInt() << std::endl; // For C++
    }

    else // Do something if it is false or zero
    {
        printf("!ReturnValueInt() did not run, and evaluates to %d \n", !returnValueInt()); // For C
        //std::cout << "ReturnValueInt did not run, and evaluates to " << !returnValueInt() << std::endl; // For C++
    }

    if ( !returnValueBool() ) // Do something if [ NOT returnValueBool() ] is true or nonzero
    {
        printf("!ReturnValueBool() ran, and evaluates to %d \n", !returnValueBool()); // For C
        //std::cout << "ReturnValueBool ran, and returned " << !returnValueBool() << std::endl; // For C++
    }
    else // Do something if it is false or zero
    {
        printf("!ReturnValueBool() did not run, and evaluates %d \n", !returnValueBool()); // For C
        //std::cout << "ReturnValueBool did not run, and evaluates to " << !returnValueBool() << std::endl; // For C++
    }

    // Call the superCustomFunction()
    int localValue = 0;                 // Dummy Value
    char aString[] = "C:\\Windows\\lmao"; // Dummy value
    int reallyImportantNumber = 42;     // Dummy Value

    long returnValue = superCustomFunction1(&localValue, aString, reallyImportantNumber); // Call the function
    printf("superCustomFunction1() Returned: %d", returnValue); // print the result (a 'long' value)

    // end program
    return 0;
}
