// MyNewHeader.cpp : This file contains the full/expanded code & functions that were forward-declared in "MyNewHeader.h".
#include "MyNewHeader.h"
/*            ^^ make sure to include the header this source file is tied to, here.
	             If there are any dependencies(libs needed) in this source, include those
				 in the header file (after header guard code) */


// v This is the full code for the forward-declared function returnValueInt() in the header file
int returnValueInt()
{
	/*

		Really Long complicated function code go here

	*/
	return 0;
}

// v This is the full code for the forward-declared function returnValueBool() in the header file
bool returnValueBool()
{
	/*
	
		Really Long complicated function code go here
	
	*/
	return false;
}

// v This is the full code forthe forward-declared function superCustomFunction1() in the header file
long superCustomFunction1(int* pointer, char* string, int value)
{
	/*
	 
		Lol nothing happens

	*/
	return (long)0;
}