// MyNewHeader.h : This file contains forward-declared code & functions defined in "MyNewHeader.h".
#pragma once
#ifndef __MY_NEW_HEADER_INCLUDE__
#define __MY_NEW_HEADER_INCLUDE__
// ^^ This #ifndef/#define stuff is a header guard to encapsulate the contents of this file (from "#define __ANY_UNIQUENAMEHERE__" to the "#endif" at the bottom)
//	  It does the same job as "#pragma once" also included here, by keeping the source from being included and compiled more than once (this can cause errors)
//	  In fact, #pragma once might be better, since it's simpler to remember & write (header guards require consistency): https://en.wikipedia.org/wiki/Pragma_once#Advantages
//    Either one of these preprocessor statements (or both) ask the compiler, "was this code already defined? If not, include the code, otherwise skip this".
//		The guard names can be anything you want, as long as it's unique and doesn't violate syntax.

int returnValueInt();	// returns an int [0], see the source file (MyNewHeader.cpp)
bool returnValueBool();	// returns a bool [false], see the source file (MyNewHeader.cpp)
// ^^ These are function forward-declarations. They are like a template of a function you're making, devoid of any actual 'functional' content
//	  "Hi compiler I have this cool function, it looks like this, but I'll tell you all about what it does somewhere else"
//	  You'll add in the contents of these functions in the complimentary source file, "MyNewHeader.cpp"

long superCustomFunction1(int* pointer, char* string, int value); // Another one, for fun

#endif //__MY_NEW_HEADER_INCLUDE__ <-- Comment not necessary, but helps in remembering the "#endif" here is tied with the "#define" guard name at the top